using de.ugoe.cs.vivian.core;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class LaserPointerInteraction : MonoBehaviour
{
    [SerializeField] protected float defaultLength = 3.0f;

    // static references to the input features required in this script
    private static readonly InputFeatureUsage<Vector3> PointerPositionFeature = new InputFeatureUsage<Vector3>("PointerPosition");
    private static readonly InputFeatureUsage<Quaternion> PointerRotationFeature = new InputFeatureUsage<Quaternion>("PointerRotation");

    // the prototype in the scene while interacting
    private VirtualPrototype[] Prototypes = null;

    // stores, wether currently the trigger is pressed
    private bool TriggerPressed = false;

    // reference to the line renderer to use
    private LineRenderer lineRenderer = null;

    // Start is called before the first frame update
    void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();

        if (lineRenderer == null)
        {
            lineRenderer = this.gameObject.AddComponent<LineRenderer>();
        }
    }

    // Update is called once every frame
    void Update()
    {
        bool triggerValue;
        InputDevice inputDevice = this.GetComponent<XRController>().inputDevice;
        Pose pose = this.GetPose(inputDevice);

        if (inputDevice.TryGetFeatureValue(CommonUsages.triggerButton, out triggerValue) && triggerValue)
        {
            this.Prototypes = FindObjectsOfType<VirtualPrototype>();

            if (this.Prototypes != null)
            {
                foreach (VirtualPrototype prototype in this.Prototypes)
                {
                    if (!this.TriggerPressed)
                    {
                        prototype.TriggerInteractionStarts(pose, ((int)inputDevice.characteristics));
                    }
                    else
                    {
                        prototype.TriggerInteractionContinues(pose, ((int)inputDevice.characteristics));
                    }
                }
            }

            this.TriggerPressed = true;
        }
        else if (this.TriggerPressed)
        {
            if (this.Prototypes != null)
            {
                foreach (VirtualPrototype prototype in this.Prototypes)
                {
                    prototype.TriggerInteractionEnds(pose, ((int)inputDevice.characteristics));
                }
            }

            this.TriggerPressed = false;
        }

        UpdateLength(pose);
    }

    // determines the pointer pose of the given input device
    private Pose GetPose(InputDevice inputDevice)
    {
        Vector3 pointerPosition;
        if (!inputDevice.TryGetFeatureValue(PointerPositionFeature, out pointerPosition))
        {
            pointerPosition = this.transform.position;
        }
        else
        {
            // the position will be local to the XR rig. If this is moved inside the scene, we need to update it
            pointerPosition = this.transform.parent.TransformPoint(pointerPosition);
        }

        Quaternion pointerRotation;
        if (!inputDevice.TryGetFeatureValue(PointerRotationFeature, out pointerRotation))
        {
            pointerRotation = this.transform.rotation;
        }
        else
        {
            // the rotation will be local to the XR rig. If this is moved inside the scene, we need to update it
            pointerRotation = this.transform.parent.rotation * pointerRotation;
        }

        return new Pose(pointerPosition, pointerRotation);
    }

    // Updates the length of the pointer
    private void UpdateLength(Pose pointerPose)
    {
        lineRenderer.SetPosition(0, pointerPose.position);
        lineRenderer.SetPosition(1, CalculateEnd(pointerPose, defaultLength));
    }

    // determines the end of the pointer
    private Vector3 CalculateEnd(Pose pointerPose, float length)
    {
        RaycastHit hit = CreateForwardRaycast(pointerPose);
        Vector3 endPosition = pointerPose.position + (pointerPose.forward * length);

        if (hit.collider)
        {
            endPosition = hit.point;
        }

        return endPosition;
    }

    // used to determine, where the pointer hits something
    private RaycastHit CreateForwardRaycast(Pose pointerPose)
    {
        RaycastHit hit;
        Ray ray = new Ray(pointerPose.position, pointerPose.forward);

        Physics.Raycast(ray, out hit, defaultLength);

        return hit;
    }
}
