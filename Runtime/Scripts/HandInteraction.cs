using de.ugoe.cs.vivian.core;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.XR.Hands;

namespace de.ugoe.cs.vivian.core
{
    public class HandInteraction : MonoBehaviour
    {
        public Handedness handedness;
        private enum Gesture { PINCH, POINT, NONE };

        XRHandSubsystem handSubsystem;

        static readonly List<XRHandSubsystem> subsystemList = new List<XRHandSubsystem>();

        private GameObject interactionSphere;

        private Vector3 lastInteractionSpherePosition = Vector3.negativeInfinity;

        private LineRenderer lineRenderer;

        // the prototypes in the scene while interacting
        private VirtualPrototype[] Prototypes = new VirtualPrototype[0];

        private Dictionary<PositionableElement, Pose> positionableElementsCollisionPoses = new Dictionary<PositionableElement, Pose>();

        private bool handIsInPointInteraction = false;

        private bool handIsInPinchInteraction = false;

        private Pose pinchStartingPose = Pose.identity;

        private Gesture handGesture = Gesture.NONE;

        void Awake()
        {
            lineRenderer = GetComponent<LineRenderer>();
        }

        protected void Start()
        {
            interactionSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            interactionSphere.name = "CollisionSphere";
            interactionSphere.transform.parent = this.transform;
            interactionSphere.transform.localPosition = Vector3.zero;
            interactionSphere.transform.localScale = new Vector3(0.015f, 0.015f, 0.015f);

            SphereCollider sphereCollider = interactionSphere.GetComponent<SphereCollider>();
            sphereCollider.isTrigger = true;

            Rigidbody rb = interactionSphere.AddComponent<Rigidbody>();
            rb.isKinematic = false;
            rb.useGravity = false;

            interactionSphere.AddComponent<ColliderReaction>().delegateHandInteraction = this;
        }

        protected void OnDisable()
        {
            UnsubscribeFromHandSubsystem();
            handSubsystem = null;
        }

        protected void Update()
        {
            EnsureHandSubsystemReferenceAndSubscription();
            UpdateHandState();
            EnsureVirtualPrototypes();

            if (!handIsInPointInteraction)
            {
                HandlePinchInteraction();
            }
        }

        void EnsureHandSubsystemReferenceAndSubscription()
        {
            if (handSubsystem != null && handSubsystem.running)
            {
                return;
            }

            SubsystemManager.GetSubsystems(subsystemList);
            for (var i = 0; i < subsystemList.Count; ++i)
            {
                var handSubsystem = subsystemList[i];
                if (handSubsystem.running)
                {
                    UnsubscribeFromHandSubsystem();
                    this.handSubsystem = handSubsystem;
                    break;
                }
            }

            SubscribeToHandSubsystem();
        }

        void SubscribeToHandSubsystem()
        {
            if (handSubsystem == null)
                return;

            handSubsystem.updatedHands += OnUpdatedHands;
        }

        void UnsubscribeFromHandSubsystem()
        {
            if (handSubsystem == null)
                return;

            handSubsystem.updatedHands -= OnUpdatedHands;
        }

        void UpdateHandState()
        {
            if (handSubsystem == null)
                return;

            XRHand hand = this.handedness == Handedness.Left ? handSubsystem.leftHand : handSubsystem.rightHand;

            // Check if left hand is tracked
            if (hand.isTracked)
            {
                this.handGesture = CalculateGesture(hand);
            }
        }

        /// <summary>
        /// Calculates a rough "openness" measure by averaging 
        /// the distance between the wrist and each fingertip.
        /// </summary>
        private Gesture CalculateGesture(XRHand hand)
        {
            if (isPinchGesture(hand))
            {
                return Gesture.PINCH;
            }
            else if (isPointGesture(hand))
            {
                return Gesture.POINT;
            }

            return Gesture.NONE;
        }

        private bool isPinchGesture(XRHand hand)
        {
            // We'll measure the distance from the wrist to each finger tip.
            // Joints we consider "tips": Thumb, Index, Middle, Ring, Little
            XRHandJointID[] fingerTipIds = new XRHandJointID[]
            {
                XRHandJointID.ThumbTip,
                XRHandJointID.IndexTip,
            };

            // Get wrist position
            Vector3 thumbPos = GetJointPosition(hand, XRHandJointID.ThumbTip);
            Vector3 indexPos = GetJointPosition(hand, XRHandJointID.IndexTip);

            float distance = Vector3.Distance(thumbPos, indexPos);

            return distance < 0.03f;
        }

        private bool isPointGesture(XRHand hand)
        {
            // We'll measure the distance from the wrist to each finger tip, exept the index finger
            XRHandJointID[] fingerTipIds = new XRHandJointID[]
            {
                XRHandJointID.MiddleTip,
                XRHandJointID.RingTip,
                XRHandJointID.LittleTip
            };

            // Get wrist position
            Vector3 wristPos = GetJointPosition(hand, XRHandJointID.Palm);

            float sumDistances = 0f;
            int validTips = 0;

            foreach (var tipId in fingerTipIds)
            {
                Vector3 tipPos = GetJointPosition(hand, tipId);
                if (tipPos != Vector3.zero)
                {
                    sumDistances += Vector3.Distance(wristPos, tipPos);
                    validTips++;
                }
            }

            if (validTips == 0)
                return false; // If we couldn't get any tip positions, return 0

            float averageDistance = sumDistances / validTips;

            // no we check wether the distance of those fingers to the wrist is smaller than
            // the distance of the tip of the index finger
            Vector3 indexTipPos = GetJointPosition(hand, XRHandJointID.IndexTip);

            return averageDistance < (Vector3.Distance(wristPos, indexTipPos) / 2);
        }

        /// <summary>
        /// Utility to try and get the 3D position of a particular hand joint.
        /// </summary>
        private Vector3 GetJointPosition(XRHand hand, XRHandJointID jointId)
        {
            // TryGetJoint gives us the XRHandJoint struct
            XRHandJoint joint = hand.GetJoint(jointId);
            if (joint != null)
            {
                // Then from that we try to get a Pose (which includes position/rotation)
                if (joint.TryGetPose(out Pose pose))
                {
                    return pose.position;
                }
            }
            return Vector3.zero; // If not tracked or missing data
        }

        private void EnsureVirtualPrototypes()
        {
            if (this.Prototypes.Length <= 0)
            {
                this.Prototypes = FindObjectsOfType<VirtualPrototype>();
            }
        }

        void HandlePinchInteraction()
        {
            if (HandStartedPinchInteraction())
            {
                Debug.Log("Hand started pinch interaction");

                KeyValuePair<PositionableElement, Pose> relevantElementAndCollisionPose = this.positionableElementsCollisionPoses.Last();
                Pose worldCollisionPose = relevantElementAndCollisionPose.Value;

                this.pinchStartingPose = new Pose
                {
                    position = this.interactionSphere.transform.InverseTransformPoint(worldCollisionPose.position),
                    rotation = Quaternion.Inverse(this.interactionSphere.transform.rotation) * worldCollisionPose.rotation
                };

                Pose pose = this.GetPinchPose();
                this.handIsInPinchInteraction = true;
                StartInteraction(pose);
            }
            else if (HandContinuesPinchInteraction())
            {
                //Debug.Log("Hand continues pinch interaction");
                Pose pose = this.GetPinchPose();
                ContinueInteraction(pose);
            }
            else if (HandStoppedPinchInteraction())
            {
                Debug.Log("Hand stopped pinch interaction");
                Pose pose = this.GetPinchPose();
                StopInteraction(pose);
                this.handIsInPinchInteraction = false;
            }
        }

        private bool HandStartedPinchInteraction()
        {
            return (!this.handIsInPinchInteraction) && (positionableElementsCollisionPoses.Count > 0) && (this.handGesture == Gesture.PINCH);
        }

        private bool HandContinuesPinchInteraction()
        {
            return this.handIsInPinchInteraction && /*(positionableElementsCollisionPoses.Count > 0) && */(this.handGesture == Gesture.PINCH);
        }

        private bool HandStoppedPinchInteraction()
        {
            return this.handIsInPinchInteraction && (/*(positionableElementsCollisionPoses.Count <= 0) ||*/ (this.handGesture != Gesture.PINCH));
        }

        void OnUpdatedHands(XRHandSubsystem subsystem, XRHandSubsystem.UpdateSuccessFlags updateSuccessFlags, XRHandSubsystem.UpdateType updateType)
        {
            XRHand hand = this.handedness == Handedness.Left ? subsystem.leftHand : subsystem.rightHand;

            var joint = hand.GetJoint(XRHandJointID.IndexTip);

            if (!joint.TryGetPose(out var pose))
            {
                //Debug.Log("could not get pose of index Finger");
                return;
            }

            lastInteractionSpherePosition = interactionSphere.transform.position;

            //Debug.Log(pose);
            interactionSphere.transform.localPosition = pose.position;
            interactionSphere.transform.localRotation = pose.rotation;

            // adapt the position minimally, as the joint post is more on the top of the index finger.
            interactionSphere.transform.Translate(Vector3.down * 0.002f);
        }

        // called when colliding with another object
        public void OnTriggerEnter(Collider collider)
        {
            if (ObjectIsPositionableElementOfVirtualPrototype(collider))
            {
                Debug.Log("collided with positionable element " + collider.name);
                StoreObjectAndCollisionPose(collider);
            }
            else if (ObjectIsInteractionElementOfVirtualPrototype(collider))
            {
                Debug.Log("collided with non-positionable element " + collider.name);
                ConsiderStartingInteractionWithNonPositionableElement();
            }
        }

        private bool ObjectIsPositionableElementOfVirtualPrototype(Collider collider)
        {
            return collider.GetComponent<PositionableElement>() != null;
        }

        private bool ObjectIsInteractionElementOfVirtualPrototype(Collider collider)
        {
            return collider.GetComponent<InteractionElement>() != null;
        }

        private void StoreObjectAndCollisionPose(Collider collider)
        {
            PositionableElement element = collider.GetComponent<PositionableElement>();
            Vector3 preCollisionPosition = this.lastInteractionSpherePosition;
            Vector3 destinationPosition = this.interactionSphere.transform.position;

            RotatableElement rotatable = collider.GetComponent<RotatableElement>();
            if (rotatable != null)
            {
                // for rotatables, we need to be a bit more specific. We ensure to cast a ray, that is going through the collider
                // and into the direction of the rotation axis
                destinationPosition = rotatable.transform.TransformPoint(rotatable.Spec.RotationAxis.Origin);
                Vector3 directionThroughCenterOfCollider = destinationPosition - collider.bounds.center;
                Vector3 pointOnOtherSideOfColliderButOnDirection = destinationPosition - directionThroughCenterOfCollider - directionThroughCenterOfCollider.normalized * collider.bounds.size.magnitude;
                Vector3 pointJustOnOtherSideOfCollider = collider.ClosestPointOnBounds(pointOnOtherSideOfColliderButOnDirection) - directionThroughCenterOfCollider.normalized * 0.01f;
                preCollisionPosition = pointJustOnOtherSideOfCollider;
            }

            Vector3 collisionDirection = (destinationPosition - preCollisionPosition).normalized;
            Quaternion rotation = Quaternion.LookRotation(collisionDirection, this.interactionSphere.transform.up);

            Pose collisionPose = new Pose(preCollisionPosition, rotation);
            this.positionableElementsCollisionPoses.Add(element, collisionPose);
        }

        // called when leaving the collider of another object
        public void OnTriggerExit(Collider collider)
        {
            if (ObjectIsPositionableElementOfVirtualPrototype(collider))
            {
                Debug.Log("uncollided with positionable element " + collider.name);
                PositionableElement element = collider.GetComponent<PositionableElement>();
                this.positionableElementsCollisionPoses.Remove(element);
            }
            else if (ObjectIsInteractionElementOfVirtualPrototype(collider))
            {
                Debug.Log("uncollided with non-positionable element " + collider.name);
                ConsiderStoppingInteractionWithNonPositionableElement();
            }
        }

        private void ConsiderStartingInteractionWithNonPositionableElement()
        {
            if (!this.handIsInPinchInteraction && !this.handIsInPointInteraction && (this.handGesture == Gesture.POINT))
            {
                this.handIsInPointInteraction = true;
                StartInteraction(GetPointPose());
            }
        }

        private void ConsiderStoppingInteractionWithNonPositionableElement()
        {
            if (this.handIsInPointInteraction)
            {
                StopInteraction(GetPointPose());
                this.handIsInPointInteraction = false;
            }
        }

        private void StartInteraction(Pose pose)
        {
            Debug.Log("starting interaction " + this.handGesture + "  " + pose.position + "  " + pose.rotation.eulerAngles);

            foreach (VirtualPrototype prototype in this.Prototypes)
            {
                prototype.TriggerInteractionStarts(pose, ((int)handedness));
            }
        }

        private void ContinueInteraction(Pose pose)
        {
            Debug.Log("continuing interaction " + this.handGesture + "  " + pose.position + "  " + pose.rotation.eulerAngles);

            foreach (VirtualPrototype prototype in this.Prototypes)
            {
                prototype.TriggerInteractionContinues(pose, ((int)handedness));
            }
        }

        private void StopInteraction(Pose pose)
        {
            Debug.Log("stopping interaction " + this.handGesture + "  " + pose.position + "  " + pose.rotation.eulerAngles);

            foreach (VirtualPrototype prototype in this.Prototypes)
            {
                prototype.TriggerInteractionEnds(pose, ((int)handedness));
            }
        }

        private Pose GetPointPose()
        {
            Quaternion effectiveRotation = Quaternion.AngleAxis(40f, this.interactionSphere.transform.right) * this.interactionSphere.transform.rotation;
            Vector3 direction = effectiveRotation * Vector3.forward;
            Vector3 origin = this.interactionSphere.transform.position - (direction.normalized * 0.03f);

            Pose result = new Pose(origin, effectiveRotation);

            VisualizePose(result);

            return result;
        }

        private Pose GetPinchPose()
        {
            Pose result = new Pose
            {
                position = this.interactionSphere.transform.TransformPoint(this.pinchStartingPose.position),
                rotation = this.interactionSphere.transform.rotation * this.pinchStartingPose.rotation
            };

            VisualizePose(result);

            return result;
        }

        private void VisualizePose(Pose pose)
        {
            Debug.DrawLine(pose.position, pose.position + pose.forward, Color.green);

            if (lineRenderer != null)
            {
                lineRenderer.SetPosition(0, pose.position);
                lineRenderer.SetPosition(1, pose.position + pose.forward * 10);
            }
        }

    }

    public class ColliderReaction : MonoBehaviour
    {
        public HandInteraction delegateHandInteraction;

        public void OnTriggerEnter(Collider other)
        {
            delegateHandInteraction.OnTriggerEnter(other);
        }

        public void OnTriggerExit(Collider other)
        {
            delegateHandInteraction.OnTriggerExit(other);
        }
    }
}
