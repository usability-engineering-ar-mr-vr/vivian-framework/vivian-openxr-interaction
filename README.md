# Vivian OpenXR Interaction

## Description
This is the plugin for Open XR based interaction for the Vivian Framework. It allows displaying virtual prototypes using the Vivian Core and interacting with them using an XR based device, such as the Meta Quest or the Magic Leap.

## Installation
This project is set up as a Unity Package. You may install it into any Unity project via the Package Manager using the "+" button and then selecting "Add package from git URL..."

## Usage
If you want to use this type of interaction, create a typical XR Rig in your scene and replace the controlers with the provided prefabs.

## Support
In case of issues, just contact the authors of the component.

## Authors
Patrick Harms, Nuremberg Institute of Technology

## License
This project is licensed open source using the Apache 2.0 license.

## Project status
There is not much to be done about this package, only in case Unity or its Open XR Interaction implementation change significantly.